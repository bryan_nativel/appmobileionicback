import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class EventEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  picture: string;

  @Column()
  title: string;

  @Column()
  resume: string;

  @Column()
  date: string;

  @Column()
  typeEvents: string;
  @Column()
  lieu: string;
}
