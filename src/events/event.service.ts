import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UpdateResult, DeleteResult } from 'typeorm';
import { EventEntity } from './event.entity';

@Injectable()
export class EventService {
  constructor(
    @InjectRepository(EventEntity)
    private eventRepository: Repository<EventEntity>,
  ) {}
  async findAll(): Promise<EventEntity[]> {
    return await this.eventRepository.find();
  }

  async create(eventEntity: EventEntity): Promise<EventEntity> {
    return await this.eventRepository.save(eventEntity);
  }

  async update(eventEntity: EventEntity): Promise<UpdateResult> {
    return await this.eventRepository.update(eventEntity.id, eventEntity);
  }

  async delete(id): Promise<DeleteResult> {
    return await this.eventRepository.delete(id);
  }
}
