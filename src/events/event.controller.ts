import { EventEntity } from './event.entity';
import { Controller, Get } from '@nestjs/common';
import { Post, Put, Delete, Body, Param } from '@nestjs/common';
import { EventService } from './event.service';

@Controller('event')
export class EventController {
  constructor(private Service: EventService) {}
  @Get()
  index(): Promise<EventEntity[]> {
    return this.Service.findAll();
  }
  @Post('create')
  async create(@Body() eventData: EventEntity): Promise<any> {
    return this.Service.create(eventData);
  }
  @Put(':id/update')
  async update(@Param('id') id, @Body() eventData: EventEntity): Promise<any> {
    eventData.id = Number(id);
    console.log('Update #' + eventData.id);
    return this.Service.update(eventData);
  }
  @Delete(':id/delete')
  async delete(@Param('id') id): Promise<any> {
    return this.Service.delete(id);
  }
}
