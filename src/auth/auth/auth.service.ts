import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { User } from '../user.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}
  private async validate(userData: User): Promise<User> {
    return await this.userService.findByEmail(userData.email);
  }

  public async login(user: User): Promise<any | { status: number }> {
    return this.validate(user).then(userData => {
      if (!userData) {
        return { status: 404 };
      }
      const payload = `${userData.fakeName}${userData.id}`;
      const accessToken = this.jwtService.sign(payload);

      return {
        // eslint-disable-next-line @typescript-eslint/camelcase
        expires_in: 3600,
        // eslint-disable-next-line @typescript-eslint/camelcase
        access_token: accessToken,
        // eslint-disable-next-line @typescript-eslint/camelcase
        user_id: payload,
        status: 200,
      };
    });
  }

  public async register(user: User): Promise<any> {
    return this.userService.create(user);
  }
}
